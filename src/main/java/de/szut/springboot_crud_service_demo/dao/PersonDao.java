package de.szut.springboot_crud_service_demo.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import de.szut.springboot_crud_service_demo.model.Person;

@Repository
public class PersonDao {

	Logger logger = LoggerFactory.getLogger(PersonDao.class);

	private static Map<Integer, Person> personList = new HashMap<>();
	
	public PersonDao() {
		initHashMap();
	}

	/**
	 * Initialisiert die HashMap, die zur Datenhaltung genutzt wird.
	 */
	private void initHashMap() {
		Person p1 = new Person(1, "Bernd", "Heidemann");
		Person p2 = new Person(2, "Sören", "Schwerk");
		Person p3 = new Person(3, "Kolja", "Buss");
		personList.put(p1.getId(), p1);
		personList.put(p2.getId(), p2);
		personList.put(p3.getId(), p3);
	}

	/**
	 * Legt einen neuen Datensatz für eine Person an.
	 * @param person	Die zu persistierende Person.
	 */
	public Person create(Person person) {
		logger.info("PersonDao.create(): " + person.getId());
		personList.put(person.getId(), person);
		return person;
	}

	/**
	 * Liest eine Person.
	 * @param id	Die ID der gesuchten Person.
	 * @return	Die gesuchte Person oder NULL.
	 */
	public Person read(int id) {
		logger.info("PersonDao.read(): " + id);
		return personList.get(id);
	}

	/**
	 * Liest alle Personen.
	 * @return	Alle persistierten Personen.
	 */
	public List<Person> read() {
		logger.info("PersonDao.read(): all");
		Collection<Person> collection = personList.values();
		List<Person> list = new ArrayList<Person>();
		list.addAll(collection);
		return list;
	}

	/**
	 * Aktualisiert eine bereits bestehende Person.
	 * @param person	Die zu aktualisierende Person.
	 */
	public Person update(Person person) {
		logger.info("PersonDao.update(): " + person.getId());
		personList.put(person.getId(), person);
		return person;
	}

	/**
	 * Löscht eine Person.
	 * @param id	Die ID der zu löschenden Person.
	 */
	public void delete(int id) {
		logger.info("Server.PersonDao.delete(): " + id);
		personList.remove(id);
	}

}
