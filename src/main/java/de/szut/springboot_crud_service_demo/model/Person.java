package de.szut.springboot_crud_service_demo.model;

public class Person {
	
	private int id;
	private String firstname;
	private String surname;

	public Person() {
	}

	public Person(int id, String firstname, String surname) {
		this.id = id;
		this.firstname = firstname;
		this.surname = surname;
	}

	public int getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getSurname() {
		return surname;
	}

}
